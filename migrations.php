<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration;

require_once __DIR__ . '/bootstrap.php';

// https://www.doctrine-project.org/projects/doctrine-migrations/en/3.3/reference/configuration.html#configuration

$metadataStorageConfiguration = new TableMetadataStorageConfiguration();
$metadataStorageConfiguration->setTableName('doctrine_migration_versions');
$metadataStorageConfiguration->setVersionColumnName('version');
$metadataStorageConfiguration->setVersionColumnLength(1024);
$metadataStorageConfiguration->setExecutedAtColumnName('executed_at');
$metadataStorageConfiguration->setExecutionTimeColumnName('execution_time');

$configuration = new Configuration();
$configuration->setMetadataStorageConfiguration($metadataStorageConfiguration);
$configuration->addMigrationsDirectory(
    'Arnapou\\Todolist\\Db\\Migrations',
    __DIR__ . '/src/Db/Migrations'
);
$configuration->setAllOrNothing(true);
$configuration->setCheckDatabasePlatform(true);

return $configuration;
