Todolist
=========

Ce projet est un **projet d'exercice** qui n'est pas amené à évoluer.

L'objectif est pour moi de pouvoir présenter une portion de mes compétences Php en mai 2022 sur un temps limité.

Temps passé au total = 13h : 

- 9 mai 4h
- 10 mai 4h
- 11 mai 3h
- 13 mai 2h


> Ce projet est largement améliorable.
> 
> Le but était pour moi de fournir une archi from scratch qui puisse être évolutive 
> avec un maximum de qualité dans un temps de dev le plus court possible.


Technique & qualité
--------------------

- Php 8.1
- `declare(strict_types=1)`
- [`vimeo/psalm`](https://packagist.org/packages/vimeo/psalm) level 1 : analyse statique de niveau le plus sévère pour cet outil
- [`̀friendsofphp/php-cs-fixer`](https://packagist.org/packages/friendsofphp/php-cs-fixer) : code style
- [CI github afférente](.gitlab-ci.yml)
- deploy auto via webhook maison sur mon serveur
- bdd sqlite


Dépendances
------------

- Pas de framework
- [`doctrine/migrations`](https://packagist.org/packages/doctrine/migrations) : setup automatique de la bdd sqlite
- [`monolog/monolog`](https://packagist.org/packages/monolog/monolog) : logging
- [`symfony/http-foundation`](https://packagist.org/packages/symfony/http-foundation) : request & response
- [`twig/twig`](https://packagist.org/packages/twig/twig) : templating html
- [`psr/log`](https://packagist.org/packages/psr/log) : [PSR-3](https://www.php-fig.org/psr/psr-3/) de logging


Elements de syntaxe moderne
----------------------------

- enums
- match
- readonly properties
- typage fort (union type, properties, ...)
- new in initializer
- attributes
- coalesce assign
- named arguments
- stringable interface

entre autres ...


"Crash" handling
-----------------

- cablage de l'error handler php qui throw des exceptions en cas d'erreur de niveau Error
- cablage de l'exception handler qui logguent toutes les exceptions non catchées
- cablage du shutdown handler en ramasse-miette pour les cas d'error
- logging des 401/404/500


Elements d'architecture backend
--------------------------------

- routing maison à l'aide d'attributes et de reflection
- gestion des 401/404/500 avec distinction entre front et api dans 2 controllers distincts
- gestion basique de signature via un header `Authorization` pour réduire les attaques directes non sollicitées sur l'api
- l'api utilise corectement les verbes https pour ses routes : GET/PATCH/POST/DELETE
- classe de services lazy loadés statiquement (vu la taille du projet : KISS)
- à chaque fois que c'était possible : injection de dépendances


Elements d'architecture frontend
--------------------------------

Cette partie s'éloigne du php donc j'ai fait du quick & dirty.

- bootstrap 5.1
- jquery 3.6
- landing page basique qui permet à "créer" une todolist
- page de todolist basique avec le minimum vital de look et d'ergonomie (keyup Enter par exemple)


Base de donnée
----------------

- 2 tables basiques : `task` & `todolist`, contraintes d'unicité + FK/PK
- classes "d'entité" sous forme de simple dto auto-peuplé par des array par injection de named arguments ; facilement encodables en json avec les bonnes interfaces php
- un service de dao pour faire la cuisine entre les dto et la bdd
- requetes préparées


Divers
------------

- svg de favicon 100% fait maison : oui je m'amuse régulièrement avec inkscape que je connais pas mal ... :D
- hébergement ovh dans une stack docker-compose constitué en frontend d'un serveur [caddy](https://caddyserver.com/) qui utilise un backend [php fpm](https://www.php.net/manual/fr/install.fpm.php)




