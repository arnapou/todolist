<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Arnapou\Todolist\AppKernel;
use Arnapou\Todolist\Services;
use Symfony\Component\HttpFoundation\Request;

require __DIR__ . '/../bootstrap.php';

$kernel = new AppKernel(Services::logger());
$kernel->handle(Request::createFromGlobals())->send();
