<?php

$header = <<<HEADER
This file is part of the Arnapou Todolist package.

(c) Arnaud Buathier <arnaud@arnapou.net>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
HEADER;

$finder = (new PhpCsFixer\Finder())
    ->name('cron')
    ->in(__DIR__);

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(
        [
            'declare_strict_types' => true,
            '@PSR1' => true,
            '@PSR2' => true,
            '@PSR12' => true,
            '@PSR12:risky' => true,
            '@Symfony' => true,
            '@Symfony:risky' => true,
            // 👇 override @Symfony - fait péter les phpdoc @psalm
            'phpdoc_to_comment' => false,
            '@DoctrineAnnotation' => true,
            '@PHP80Migration' => true,
            '@PHP81Migration' => true,
            'concat_space' => ['spacing' => 'one'],
            'ordered_imports' => ['sort_algorithm' => 'alpha'],
            'native_function_invocation' => ['include' => ['@compiler_optimized']],
            'combine_consecutive_issets' => true,
            'combine_consecutive_unsets' => true,
            'phpdoc_order' => true,
            'phpdoc_var_annotation_correct_order' => true,
            'global_namespace_import' => [
                'import_classes' => false,
                'import_functions' => false,
                'import_constants' => false
            ],
            'header_comment' => ['header' => $header],
        ]
    )
    ->setFinder($finder);
