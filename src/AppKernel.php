<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist;

use Arnapou\Todolist\Controller\ApiController;
use Arnapou\Todolist\Controller\FrontController;
use Arnapou\Todolist\Core\Http\ApiUnauthorized;
use Arnapou\Todolist\Core\Http\PageNotFound;
use Arnapou\Todolist\Core\Http\ResponseUtils;
use Arnapou\Todolist\Core\Http\RouteUrlMatcher;
use Arnapou\Todolist\Core\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class AppKernel
{
    use ResponseUtils;

    public function __construct(private readonly Logger $logger)
    {
    }

    public function handle(Request $request): Response
    {
        if (str_starts_with($request->getPathInfo(), APP_PREFIX_API)) {
            return $this->handleApi($request);
        }

        return $this->handleFront($request);
    }

    private function handleApi(Request $request): Response
    {
        try {
            return $this->getResponse(new ApiController($request), $request);
        } catch (ApiUnauthorized $throwable) {
            return $this->errorJsonResponse($throwable, 401);
        } catch (PageNotFound $throwable) {
            return $this->errorJsonResponse($throwable, 404);
        } catch (\Throwable $throwable) {
            return $this->errorJsonResponse($throwable, 500);
        }
    }

    private function handleFront(Request $request): Response
    {
        try {
            return $this->getResponse(new FrontController($request), $request);
        } catch (PageNotFound $throwable) {
            return $this->errorHtmlResponse($throwable, 404);
        } catch (\Throwable $throwable) {
            return $this->errorHtmlResponse($throwable, 500);
        }
    }

    private function errorJsonResponse(\Throwable $throwable, int $code): JsonResponse
    {
        $this->logger->throwable($throwable);

        $response = new JsonResponse([
            'error_message' => $throwable->getMessage(),
            'error_code' => $code,
        ]);
        $response->setStatusCode($code);

        return $response;
    }

    private function errorHtmlResponse(\Throwable $throwable, int $code): Response
    {
        $this->logger->throwable($throwable);

        $response = $this->render("error$code.twig", ['throwable' => $throwable]);
        $response->setStatusCode($code);

        return $response;
    }

    private function getResponse(object $controller, Request $request): Response
    {
        foreach (new RouteUrlMatcher($controller, $request) as $method) {
            $response = [$controller, $method->getName()]();

            if (!$response instanceof Response) {
                throw new \RuntimeException('Houston, we have a problem ! This MUST NOT occur.');
            }

            return $response;
        }

        throw new PageNotFound();
    }
}
