<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist;

use Arnapou\Todolist\Model\Todolist;

final class AppSecurity
{
    public function verifyAuthorization(?string $authorization, Todolist $todolist): bool
    {
        return $this->getAuthorization($todolist) === $authorization;
    }

    public function getAuthorization(Todolist $todolist): string
    {
        $id = $todolist->id;
        $name = (string) $todolist->name;

        return hash_hmac('sha256', "$id.$name", APP_SECRET);
    }
}
