<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist;

use Arnapou\Todolist\Core\Logger;
use Arnapou\Todolist\Core\PhpHandlers;
use Arnapou\Todolist\Db\DAO;

/**
 * This is a full static class for lazy loaded services.
 *
 * KISS for such a light project & strong typing.
 */
final class Services
{
    private static ?\PDO $db = null;
    private static ?DAO $dao = null;
    private static ?Logger $logger = null;
    private static ?PhpHandlers $phpHandlers = null;
    private static ?\Twig\Environment $twig = null;
    private static ?\Monolog\Logger $monolog = null;

    private function __construct()
    {
    }

    public static function db(): \PDO
    {
        return self::$db ??= new \PDO(
            dsn: 'sqlite:' . APP_PATH_SQLITE,
            options: [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            ]
        );
    }

    public static function dao(): DAO
    {
        return self::$dao ??= new DAO(self::db());
    }

    public static function logger(): Logger
    {
        return self::$logger ??= new Logger(self::monolog(), self::phpHandlers());
    }

    public static function phpHandlers(): PhpHandlers
    {
        return self::$phpHandlers ??= new PhpHandlers(self::monolog());
    }

    public static function twig(): \Twig\Environment
    {
        return self::$twig ??= new \Twig\Environment(
            loader: new \Twig\Loader\FilesystemLoader(APP_PATH_TEMPLATES),
            options: [
                'strict_variables' => false,
                'cache' => APP_PATH_CACHE,
                'auto_reload' => true,
                'optimizations' => -1,
            ]
        );
    }

    /**
     * External logger we don't want to expose.
     */
    private static function monolog(): \Monolog\Logger
    {
        return self::$monolog ??= new \Monolog\Logger(
            name: APP_NAME,
            handlers: [new \Monolog\Handler\RotatingFileHandler(filename: APP_PATH_LOGS . '/app.log', maxFiles: 7)]
        );
    }
}
