<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Controller;

use Arnapou\Todolist\AppSecurity;
use Arnapou\Todolist\Core\Http\ResponseUtils;
use Arnapou\Todolist\Core\Http\Verb;
use Arnapou\Todolist\Core\Info\RouteInfo;
use Arnapou\Todolist\Model\TaskStatus;
use Arnapou\Todolist\Model\TodolistName;
use Arnapou\Todolist\Services;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class FrontController
{
    use ResponseUtils;

    public function __construct(private readonly Request $request)
    {
    }

    #[RouteInfo('/', Verb::GET)]
    public function indexAction(): Response
    {
        try {
            $name = new TodolistName((string) $this->request->query->get('todolist'));

            $context = [
                'apiurl' => APP_PREFIX_API,
                'todolist' => $todolist = Services::dao()->findOrCreateTodolist($name),
                'authorization' => (new AppSecurity())->getAuthorization($todolist),
                'statuses' => TaskStatus::toArray(),
            ];

            return $this->render('todolist.twig', $context);
        } catch (\TypeError) {
            return $this->render('index.twig');
        }
    }

    #[RouteInfo('/new', Verb::GET)]
    public function newAction(): Response
    {
        return $this->redirect('/?' . http_build_query(['todolist' => (string) new TodolistName()]));
    }
}
