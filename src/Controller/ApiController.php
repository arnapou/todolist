<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Controller;

use Arnapou\Todolist\AppSecurity;
use Arnapou\Todolist\Core\Http\ApiCannotPerformAction;
use Arnapou\Todolist\Core\Http\ApiUnauthorized;
use Arnapou\Todolist\Core\Http\ApiUndefinedTodolist;
use Arnapou\Todolist\Core\Http\Verb;
use Arnapou\Todolist\Core\Info\ControllerInfo;
use Arnapou\Todolist\Core\Info\RouteInfo;
use Arnapou\Todolist\Db\DAO;
use Arnapou\Todolist\Model\Task;
use Arnapou\Todolist\Model\TaskStatus;
use Arnapou\Todolist\Model\Todolist;
use Arnapou\Todolist\Model\TodolistName;
use Arnapou\Todolist\Services;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

#[ControllerInfo(prefix: APP_PREFIX_API)]
final class ApiController
{
    private DAO $dao;

    public function __construct(
        private readonly Request $request,
        private readonly AppSecurity $security = new AppSecurity(),
    ) {
        $this->dao = Services::dao();
    }

    #[RouteInfo('/task', Verb::DELETE)]
    public function deleteTaskAction(): JsonResponse
    {
        $todolist = $this->getCurrentTodolist();
        $task = $this->dao->findTask($todolist, (int) $this->request->request->get('id'));
        if (null === $task) {
            throw new ApiCannotPerformAction('Unkwown task');
        }

        $this->dao->delete($task);

        return new JsonResponse($task);
    }

    #[RouteInfo('/task', Verb::POST)]
    public function createTaskAction(): JsonResponse
    {
        $todolist = $this->getCurrentTodolist();
        if (null === $todolist->id) {
            throw new ApiCannotPerformAction('Unkwown todolist');
        }

        $task = new Task($todolist->id);
        $this->updateTaskFromRequest($task);
        $this->dao->save($task);

        return new JsonResponse($task);
    }

    #[RouteInfo('/task', Verb::PATCH)]
    public function updateTaskAction(): JsonResponse
    {
        $todolist = $this->getCurrentTodolist();
        $taskId = (int) $this->request->request->get('id');
        if (null === $todolist->id || empty($taskId)) {
            throw new ApiCannotPerformAction('Unkwown todolist or task');
        }

        $task = $this->dao->findTask($todolist, $taskId);
        if (null === $task) {
            throw new ApiCannotPerformAction('Unkwown task');
        }

        $this->updateTaskFromRequest($task);
        $this->dao->save($task);

        return new JsonResponse($task);
    }

    #[RouteInfo('/tasks', Verb::GET)]
    public function getTasksAction(): JsonResponse
    {
        $todolist = $this->getCurrentTodolist();
        $tasks = $this->dao->findTasks($todolist);

        return new JsonResponse($tasks);
    }

    /**
     * Auto create todolist if it does not exists.
     */
    private function getCurrentTodolist(): Todolist
    {
        $authorization = $this->request->headers->get('Authorization');
        $name = new TodolistName(
            Request::METHOD_GET === $this->request->getMethod()
                ? (string) $this->request->query->get('todolist')
                : (string) $this->request->request->get('todolist')
        );
        $todolist = $this->dao->findTodolist($name);

        if (null === $todolist) {
            throw new ApiUndefinedTodolist('Undefined Todolist');
        }

        if (!$this->security->verifyAuthorization($authorization, $todolist)) {
            throw new ApiUnauthorized('Undefined Todolist');
        }

        return $todolist;
    }

    /**
     * Update the task from request data (not GET, obviously).
     */
    private function updateTaskFromRequest(Task $task): void
    {
        if ($status = TaskStatus::tryFrom((string) $this->request->request->get('status'))) {
            $task->status = $status;
        }

        if ($value = (string) $this->request->request->get('description')) {
            $task->description = $value;
        }
    }
}
