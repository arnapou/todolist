<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Model;

final class Task extends BaseModel
{
    public TaskStatus $status;

    public function __construct(
        public readonly int $todolistId,
        public string $description = '',
        ?string $status = null,
        ?int $id = null,
        ?int $createdAt = null,
        ?int $modifiedAt = null,
    ) {
        parent::__construct($id, $createdAt, $modifiedAt);

        $this->status = TaskStatus::tryFrom((string) $status) ?? TaskStatus::todo;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'todolistId' => $this->todolistId,
            'description' => $this->description,
            'status' => $this->status->value,
            'createdAt' => $this->createdAt->getTimestamp(),
            'modifiedAt' => $this->modifiedAt->getTimestamp(),
        ];
    }
}
