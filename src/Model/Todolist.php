<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Model;

final class Todolist extends BaseModel
{
    public TodolistName $name;

    public function __construct(
        TodolistName|string|\Stringable $name,
        ?int $id = null,
        ?int $createdAt = null,
        ?int $modifiedAt = null,
    ) {
        parent::__construct($id, $createdAt, $modifiedAt);

        $this->name = $name instanceof TodolistName ? $name : new TodolistName($name);
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'createdAt' => $this->createdAt->getTimestamp(),
            'modifiedAt' => $this->modifiedAt->getTimestamp(),
        ];
    }
}
