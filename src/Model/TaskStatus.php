<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Model;

enum TaskStatus: string
{
    case todo = 'todo';
    case pending = 'pending';
    case finished = 'finished';

    public static function toArray(): array
    {
        $items = [];

        foreach (self::cases() as $item) {
            $items[$item->name] = $item->value;
        }

        return $items;
    }
}
