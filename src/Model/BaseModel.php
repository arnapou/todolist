<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Model;

abstract class BaseModel implements \JsonSerializable
{
    public readonly \DateTimeImmutable $createdAt;
    public \DateTimeImmutable $modifiedAt;

    public function __construct(
        public readonly ?int $id = null,
        ?int $createdAt = null,
        ?int $modifiedAt = null,
    ) {
        $this->createdAt = $this->timestampToDateTimeImmutable($createdAt ?? time());
        $this->modifiedAt = $this->timestampToDateTimeImmutable($modifiedAt ?? time());
    }

    final public function updateModifiedAt(): void
    {
        $this->modifiedAt = $this->timestampToDateTimeImmutable(time());
    }

    private function timestampToDateTimeImmutable(int $timestamp): \DateTimeImmutable
    {
        return \DateTimeImmutable::createFromMutable((new \DateTime())->setTimestamp($timestamp));
    }

    abstract public function jsonSerialize(): array;

    final public static function fromArray(array $array): static
    {
        /**
         * @psalm-suppress UnsafeInstantiation
         * @psalm-suppress PossiblyInvalidArgument
         * @psalm-suppress MixedArgument
         */
        return new static(...$array);
    }

    final public function toArray(): array
    {
        return $this->jsonSerialize();
    }
}
