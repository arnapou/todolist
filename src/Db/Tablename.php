<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Db;

use Arnapou\Todolist\Model\BaseModel;
use Arnapou\Todolist\Model\Task;
use Arnapou\Todolist\Model\Todolist;

final class Tablename implements \Stringable
{
    private const MAPPING = [
        Task::class => 'task',
        Todolist::class => 'todolist',
    ];

    public readonly string $value;

    public function __construct(string|BaseModel $modelOrClass)
    {
        if ($modelOrClass instanceof BaseModel) {
            $class = \get_class($modelOrClass);
        } elseif (is_subclass_of($modelOrClass, BaseModel::class, true)) {
            $class = $modelOrClass;
        } else {
            throw new NotAModel();
        }

        $this->value = self::MAPPING[$class] ?? throw new UnknownTablename();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
