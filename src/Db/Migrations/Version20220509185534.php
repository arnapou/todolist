<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Db\Migrations;

use Arnapou\Todolist\Db\Tablename;
use Arnapou\Todolist\Model\Task;
use Arnapou\Todolist\Model\Todolist;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220509185534 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initialization';
    }

    public function up(Schema $schema): void
    {
        $table = new Tablename(Todolist::class);
        $this->addSql(
            <<<SQL
                create table $table
                (
                    id         integer constraint list_pk   primary key,
                    createdAt  integer not null,
                    modifiedAt integer not null,
                    name       text    constraint list_name unique
                );
                SQL
        );

        $table = new Tablename(Task::class);
        $this->addSql(
            <<<SQL
                create table $table
                (
                    id          integer constraint task_pk primary key,
                    todolistId  integer references todolist,
                    createdAt   integer not null,
                    modifiedAt  integer not null,
                    description text,
                    status      text
                );
                SQL
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table if exists ' . new Tablename(Todolist::class));
        $this->addSql('drop table if exists ' . new Tablename(Task::class));
    }
}
