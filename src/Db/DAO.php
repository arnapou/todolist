<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Db;

use Arnapou\Todolist\Model\BaseModel;
use Arnapou\Todolist\Model\Task;
use Arnapou\Todolist\Model\Todolist;
use Arnapou\Todolist\Model\TodolistName;

final class DAO
{
    public function __construct(private readonly \PDO $db)
    {
    }

    /**
     * Find a Todolist object by its unique name.
     */
    public function findTodolist(TodolistName $name): ?Todolist
    {
        $sql = 'SELECT * FROM ' . new Tablename(Todolist::class) . ' WHERE name=:name';

        $statement = $this->db->prepare($sql);
        $statement->execute(['name' => $name]);
        if (empty($data = $statement->fetchAll())) {
            return null;
        }

        /** @psalm-suppress MixedArgument */
        return new Todolist(...$data[0]);
    }

    /**
     * @return Task[]
     */
    public function findTasks(Todolist $todolist): array
    {
        if (null === $todolist->id) {
            return [];
        }

        $sql = 'SELECT * FROM ' . new Tablename(Task::class)
            . ' WHERE todolistId=:todolistId ORDER BY modifiedAt DESC';

        $statement = $this->db->prepare($sql);
        $statement->execute(['todolistId' => $todolist->id]);

        $tasks = [];

        /** @psalm-var array $row */
        foreach ($statement as $row) {
            $tasks[] = Task::fromArray($row);
        }

        return $tasks;
    }

    public function findTask(Todolist $todolist, ?int $id): ?Task
    {
        if (null === $todolist->id || null === $id) {
            return null;
        }

        $sql = 'SELECT * FROM ' . new Tablename(Task::class)
            . ' WHERE todolistId=:todolistId AND id=:id ORDER BY modifiedAt DESC';

        $statement = $this->db->prepare($sql);
        $statement->execute(['todolistId' => $todolist->id, 'id' => $id]);
        if (empty($data = $statement->fetchAll())) {
            return null;
        }

        /** @psalm-suppress MixedArgument */
        return new Task(...$data[0]);
    }

    /**
     * This autocreate a Todolist.
     */
    public function findOrCreateTodolist(TodolistName $name): Todolist
    {
        if ($todolist = $this->findTodolist($name)) {
            return $todolist;
        }

        return $this->save(new Todolist($name));
    }

    /**
     * This method updates or inserts the object depending on its id.
     *
     * @template TModel of BaseModel
     *
     * @param TModel $object
     *
     * @return TModel
     */
    public function save(BaseModel $object): BaseModel
    {
        $object->updateModifiedAt();

        if ($object->id) {
            return $this->update($object);
        }

        return $this->insert($object);
    }

    /**
     * Return the same object.
     *
     * @template TModel of BaseModel
     *
     * @param TModel $object
     *
     * @return TModel
     */
    private function update(BaseModel $object): BaseModel
    {
        $table = new Tablename($object);
        $array = $object->toArray();

        $sets = array_map(
            static fn (int|string $field): string => "`$field`=:$field",
            array_keys($array)
        );

        $sql = "UPDATE $table SET " . implode(', ', $sets) . ' WHERE `id`=:id';

        $statement = $this->db->prepare($sql);
        $statement->execute($array);

        return $object;
    }

    /**
     * Return a new object with the new autoincrement id.
     *
     * @template TModel of BaseModel
     *
     * @param TModel $object
     *
     * @return TModel
     */
    private function insert(BaseModel $object): BaseModel
    {
        $table = new Tablename($object);
        $array = $object->toArray();

        $keys = array_keys($array);
        $fields = array_map(static fn (int|string $field): string => "`$field`", $keys);
        $values = array_map(static fn (int|string $field): string => ":$field", $keys);

        $sql = "INSERT INTO $table (" . implode(',', $fields) . ') VALUES (' . implode(',', $values) . ')';

        $statement = $this->db->prepare($sql);
        $statement->execute($array);

        $id = $this->db->lastInsertId();

        return $object::fromArray(['id' => (int) $id] + $array);
    }

    public function delete(BaseModel $object): void
    {
        $table = new Tablename($object);

        $sql = "DELETE FROM $table WHERE id=:id";

        $statement = $this->db->prepare($sql);
        $statement->execute(['id' => $object->id]);
    }
}
