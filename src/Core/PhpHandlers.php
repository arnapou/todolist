<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Core;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

final class PhpHandlers
{
    private const SEVERITY_ERROR_FOR_THROW = [
        \E_ERROR,
        \E_PARSE,
        \E_CORE_ERROR,
        \E_CORE_WARNING,
        \E_COMPILE_ERROR,
        \E_COMPILE_WARNING,
        \E_USER_ERROR,
        \E_RECOVERABLE_ERROR,
    ];

    private bool $registered = false;

    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function register(): void
    {
        if (!$this->registered) {
            $this->registered = true;

            set_error_handler($this->getErrorHandler());
            set_exception_handler($this->getExceptionHandler());
            register_shutdown_function($this->getShutdownHandler());
        }
    }

    public function logThrowable(\Throwable $throwable): void
    {
        $this->logger->log(
            $this->throwableToLogLevel($throwable),
            $this->throwableToString($throwable),
            $this->throwableToContext($throwable)
        );
    }

    private function getErrorHandler(): \Closure
    {
        return function (int $errno, string $errstr, string $errfile, int $errline): void {
            $error = new \ErrorException($errstr, $errno, $errno, $errfile, $errline);

            $this->logThrowable($error);
            if (\in_array($errno, self::SEVERITY_ERROR_FOR_THROW, true)) {
                throw $error;
            }
        };
    }

    private function getExceptionHandler(): \Closure
    {
        return function (\Throwable $throwable): void {
            $this->logThrowable($throwable);
        };
    }

    private function getShutdownHandler(): \Closure
    {
        return function (): void {
            if (($err = error_get_last()) && ($msg = $err['message'])) {
                $this->logThrowable(
                    new \ErrorException($msg, $err['type'], $err['type'], $err['file'], $err['line'])
                );
            }
        };
    }

    private function throwableToLogLevel(\Throwable $throwable): string
    {
        if ($throwable instanceof \Error) {
            return LogLevel::ERROR;
        }

        if ($throwable instanceof \ErrorException) {
            return match ($throwable->getSeverity()) {
                \E_DEPRECATED, \E_USER_DEPRECATED, \E_STRICT => LogLevel::INFO,
                \E_NOTICE, \E_USER_NOTICE => LogLevel::NOTICE,
                \E_WARNING, \E_USER_WARNING => LogLevel::WARNING,
                default => LogLevel::ERROR,
            };
        }

        return LogLevel::CRITICAL;
    }

    private function throwableToString(\Throwable $throwable): string
    {
        return $throwable->getMessage();
    }

    private function throwableToContext(\Throwable $throwable, int $level = 0): array
    {
        $array = [
            'class' => get_debug_type($throwable),
            'file' => $throwable->getFile() . ':' . $throwable->getLine(),
        ];

        $array['code'] = $throwable instanceof \ErrorException
            ? $this->errorCodeToString($throwable->getSeverity())
            : $throwable->getCode();

        if ($level > 0) {
            $array['message'] = $throwable->getMessage();
        }

        $array['trace'] = $throwable->getTraceAsString();

        if ($level < 2 && $throwable->getPrevious()) {
            $array['previous'] = $this->throwableToContext($throwable->getPrevious(), $level + 1);
        }

        return $array;
    }

    private function errorCodeToString(int $code): string
    {
        return match ($code) {
            \E_ERROR => 'E_ERROR', // 1
            \E_WARNING => 'E_WARNING', // 2
            \E_PARSE => 'E_PARSE', // 4
            \E_NOTICE => 'E_NOTICE', // 8
            \E_CORE_ERROR => 'E_CORE_ERROR', // 16
            \E_CORE_WARNING => 'E_CORE_WARNING', // 32
            \E_COMPILE_ERROR => 'E_COMPILE_ERROR', // 64
            \E_COMPILE_WARNING => 'E_COMPILE_WARNING', // 128
            \E_USER_ERROR => 'E_USER_ERROR', // 256
            \E_USER_WARNING => 'E_USER_WARNING', // 512
            \E_USER_NOTICE => 'E_USER_NOTICE', // 1024
            \E_STRICT => 'E_STRICT', // 2048
            \E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR', // 4096
            \E_DEPRECATED => 'E_DEPRECATED', // 8192
            \E_USER_DEPRECATED => 'E_USER_DEPRECATED', // 16384
            default => 'E_UNKNOWN',
        };
    }
}
