<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Core\Http;

use Arnapou\Todolist\Core\Info\ControllerInfo;
use Arnapou\Todolist\Core\Info\RouteInfo;
use Symfony\Component\HttpFoundation\Request;

/**
 * @template-implements \IteratorAggregate<\ReflectionMethod>
 */
final class RouteUrlMatcher implements \IteratorAggregate
{
    public function __construct(
        private readonly object $controller,
        private readonly Request $request
    ) {
    }

    /**
     * @return \Generator<\ReflectionMethod>
     */
    public function getIterator(): \Generator
    {
        $prefix = $this->getControllerPrefix($this->controller);
        foreach ($this->getControllerRoutes($this->controller) as [$method, $route]) {
            if ($this->isMatching($this->request, $prefix, $route)) {
                yield $method;
            }
        }
    }

    private function isMatching(Request $request, string $prefix, RouteInfo $route): bool
    {
        return $request->getMethod() === $route->verb->value
            && $request->getPathInfo() === $prefix . ltrim($route->url, '/');
    }

    /**
     * @return \Generator<array{\ReflectionMethod, RouteInfo}>
     */
    private function getControllerRoutes(object $controller): \Generator
    {
        foreach ($this->getPublicMethods($controller) as $refMethod) {
            yield from $this->getMethodRoutes($refMethod);
        }
    }

    /**
     * @return \Generator<array{\ReflectionMethod, RouteInfo}>
     */
    private function getMethodRoutes(\ReflectionMethod $refMethod): \Generator
    {
        foreach ($refMethod->getAttributes() as $refAttribute) {
            $attribute = $refAttribute->newInstance();
            if ($attribute instanceof RouteInfo) {
                yield [$refMethod, $attribute];
            }
        }
    }

    /**
     * @return \Generator<\ReflectionMethod>
     */
    private function getPublicMethods(object $controller): \Generator
    {
        $refClass = new \ReflectionClass($controller);
        foreach ($refClass->getMethods() as $refMethod) {
            if ($refMethod->isPublic()) {
                yield $refMethod;
            }
        }
    }

    private function getControllerPrefix(object $controller): string
    {
        $refClass = new \ReflectionClass($controller);
        foreach ($refClass->getAttributes() as $refAttribute) {
            $attribute = $refAttribute->newInstance();
            if ($attribute instanceof ControllerInfo) {
                return $attribute->prefix;
            }
        }

        return '/';
    }
}
