<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arnapou\Todolist\Core;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

final class Logger implements LoggerInterface
{
    use LoggerTrait;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly PhpHandlers $phpHandlers
    ) {
    }

    public function throwable(\Throwable $throwable): void
    {
        $this->phpHandlers->logThrowable($throwable);
    }

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }
}
