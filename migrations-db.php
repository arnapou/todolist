<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Doctrine\DBAL\DriverManager;

require_once __DIR__ . '/bootstrap.php';

// https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#pdo-mysql

return DriverManager::getConnection(
    [
        'path' => APP_PATH_SQLITE,
        'driver' => 'pdo_sqlite',
    ]
);
