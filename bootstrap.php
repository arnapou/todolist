<?php

declare(strict_types=1);

/*
 * This file is part of the Arnapou Todolist package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once __DIR__ . '/vendor/autoload.php';

const APP_NAME = 'Todolist';
const APP_PATH_TEMPLATES = __DIR__ . '/templates';
const APP_PATH_CACHE = __DIR__ . '/var/cache';
const APP_PATH_LOGS = __DIR__ . '/var/logs';
const APP_PATH_SQLITE = __DIR__ . '/var/data/db.sqlite';
const APP_TIMEZONE = 'Europe/Paris';
const APP_PREFIX_API = '/api/';

// You MUST define your own secret.
// The fallback is here only for testing or demo purpose.
if ($secret = getenv('APP_SECRET')) {
    \define('APP_SECRET', (string) $secret);
} else {
    \define('APP_SECRET', 'non-blocking-fallback-for-the-application');
}

date_default_timezone_set(APP_TIMEZONE);

\Arnapou\Todolist\Services::phpHandlers()->register();
